import argparse
import os
import aiohttp
import asyncio
import aiofiles

parser = argparse.ArgumentParser()
parser.add_argument("-u", "--url", action="store")
argv = parser.parse_args()
print(argv.url)

async def get_content(url):
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as resp:
            resp = await resp.read()
            return resp

async def write_content(content, file):
    working_directory = os.getcwd()
    async with aiofiles.open(working_directory + file, "wb") as f:
        await f.write(content)
        await f.flush()

async def main():
    content = await get_content(argv.url)
    await write_content(content, "\\tmp\\async_web_page")

if __name__ == "__main__":
    asyncio.run(main())
