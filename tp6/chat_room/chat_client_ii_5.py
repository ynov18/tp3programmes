import asyncio
import aioconsole

async def send_user_input(writer):
    while True:
        user_input = await aioconsole.ainput()
        writer.write(user_input.encode())
        await writer.drain()

async def receive_server_messages(reader):
    try:
        while True:
            data = await reader.read(1024)
            if not data:
                break
            message = data.decode()
            print(message, end="", flush=True)
    except asyncio.CancelledError:
        pass

async def main():
    server_address = ('10.5.1.11', 13337)

    pseudo = input("Saisissez votre pseudo : ")

    reader, writer = await asyncio.open_connection(*server_address)

    try:
        writer.write(f"Hello|{pseudo}".encode())
        await writer.drain()

        task_send = asyncio.create_task(send_user_input(writer))
        task_receive = asyncio.create_task(receive_server_messages(reader))

        await asyncio.gather(task_send, task_receive)

    except asyncio.CancelledError:
        pass
    except KeyboardInterrupt:
        print("\nClient interrompu. Fermeture en cours...")

    finally:
        writer.close()
        await writer.wait_closed()

if __name__ == "__main__":
    asyncio.run(main())
