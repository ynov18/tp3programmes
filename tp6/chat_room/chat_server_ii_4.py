import asyncio

CLIENTS = {}

async def handle_client(reader, writer):
    try:

        client_address = writer.get_extra_info('peername')
        print(f"Client connecté : {client_address}")


        if client_address in CLIENTS:
            print("Client déjà connecté. Ignorer la nouvelle connexion.")
            return

        CLIENTS[client_address] = {
            "r": reader,
            "w": writer
        }   
        welcome_message = f"Hello {client_address[0]}:{client_address[1]}\n"
        writer.write(welcome_message.encode())
        await writer.drain()

        while True:
          
            data = await reader.read(1024)
            if not data:
                break

            message = data.decode()
            print(f"Message reçu de {client_address[0]}:{client_address[1]} : {message}")


            await broadcast_message(client_address, message)

    except BrokenPipeError:
   
        pass
    finally:
 
        print(f"Client déconnecté : {client_address}")
        del CLIENTS[client_address] 
        writer.close()
        await writer.wait_closed()

async def broadcast_message(sender_address, message):

    for client_address, client_info in CLIENTS.items():
        if client_address != sender_address:

            broadcast_msg = f"{sender_address[0]}:{sender_address[1]} a dit : {message}\n"

            client_info["w"].write(broadcast_msg.encode())
            await client_info["w"].drain()

async def main():
    server = await asyncio.start_server(
        handle_client, '10.5.1.11', 13337, reuse_address=False)

    async with server:
        await server.serve_forever()

if __name__ == "__main__":
    asyncio.run(main())
