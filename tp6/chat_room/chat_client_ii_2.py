import socket

def main():
    server_address = ('10.5.1.11', 13337)

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as client_socket:

        client_socket.connect(server_address)

        client_socket.sendall(b"Hello")

        data = client_socket.recv(1024)

        print(f"Réponse du serveur: {data.decode()}")

if __name__ == "__main__":
    main()