import asyncio

async def handle_client(reader, writer):
    try:
        client_address = writer.get_extra_info('peername')
        print(f"Client connecté : {client_address}")

        welcome_message = f"Hello {client_address[0]}:{client_address[1]}\n"
        writer.write(welcome_message.encode())
        await writer.drain()

        while True:

            data = await reader.read(1024)
            if not data:
                break


            message = data.decode()
            print(f"Message reçu de {client_address[0]}:{client_address[1]} : {message}")


            writer.write(data)
            await writer.drain()

    except BrokenPipeError:

        pass
    finally:
        print(f"Client déconnecté : {client_address}")
        writer.close()
        await writer.wait_closed()

async def main():
    server = await asyncio.start_server(
        handle_client, '10.5.1.11', 13337, reuse_address=False)

    async with server:
        await server.serve_forever()

if __name__ == "__main__":
    asyncio.run(main())
