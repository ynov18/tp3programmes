import asyncio

CLIENTS = {}

async def handle_client(reader, writer):
    try:
        client_address = writer.get_extra_info('peername')
        print(f"Client connecté : {client_address}")


        data = await reader.read(1024)
        if not data.startswith(b"Hello|"):
            print("Message de connexion invalide. Déconnexion du client.")
            return


        pseudo = data.decode().split("|")[1]

        if client_address not in CLIENTS:

            CLIENTS[client_address] = {
                "r": reader,
                "w": writer,
                "pseudo": pseudo
            }

            announce_message = f"Annonce : {pseudo} a rejoint la chatroom\n"
            await broadcast_message(client_address, announce_message)

        while True:

            data = await reader.read(1024)
            if not data:
                break

            message = data.decode()
            print(f"Message reçu de {pseudo} : {message}")

            await broadcast_message(client_address, f"{pseudo} a dit : {message}")

    except BrokenPipeError:
        
        
        pass
    finally:
      
        print(f"Client déconnecté : {client_address}")
        del CLIENTS[client_address] 

        await broadcast_message(client_address, f"Annonce : {pseudo} a quitté la chatroom\n")
        writer.close()
        await writer.wait_closed()

async def broadcast_message(sender_address, message):
    for client_address, client_info in CLIENTS.items():
        if client_address != sender_address:
            client_info["w"].write(message.encode())
            await client_info["w"].drain()

async def main():
    server = await asyncio.start_server(
        handle_client, '10.5.1.11', 13337, reuse_address=False)

    async with server:
        await server.serve_forever()

if __name__ == "__main__":
    asyncio.run(main())
