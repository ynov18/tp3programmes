import socket
import re

def est_calcul_valide(expression):
    # Définir le motif de l'expression régulière
    motif = r'^\s*(-?\d+)\s*([-+*])\s*(-?\d+)\s*$'

    # Vérifier si l'expression correspond au motif
    correspondance = re.match(motif, expression)

    if correspondance:
        # Extraire les parties de l'expression
        nombre1 = int(correspondance.group(1))
        operateur = correspondance.group(2)
        nombre2 = int(correspondance.group(3))

        # Vérifier si les nombres sont dans la plage spécifiée
        if 0 <= nombre1 <= 4294967295 and 0 <= nombre2 <= 4294967295:
            # Vérifier si l'opérateur est l'un des opérateurs autorisés
            if operateur in ['+', '-', '*']:
                return True
    return False


socke = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
socke.connect(('10.5.1.11', 13337))
# on récup une string saisie par l'utilisateur
msg = input('Enter a message: ')

while  est_calcul_valide(msg)==False:
    msg =input('veuillez entrer un calcul valide : ')


                
# on encode le message explicitement en UTF-8 pour récup un tableau de bytes
encoded_msg = msg.encode('utf-8')

# on calcule sa taille, en nombre d'octets
msg_len = len(encoded_msg)+len(b'<clafin>')

# on encode ce nombre d'octets sur une taille fixe de 4 octets
header = msg_len.to_bytes(4, byteorder='big')


# on peut concaténer ce header avec le message, avant d'envoyer sur le réseau
payload = header + encoded_msg +b'<clafin>'



# on peut envoyer ça sur le réseau
socke.send(payload)
socke.close()
