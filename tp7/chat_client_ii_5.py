import asyncio
import websockets

async def send_user_input(websocket):
    while True:
        user_input = await asyncio.to_thread(input, "Saisissez votre message : ")
        await websocket.send(user_input)

async def receive_server_messages(websocket):
    try:
        while True:
            message = await websocket.recv()
            print(message, end="", flush=True)
    except websockets.ConnectionClosedError:
        pass

async def main():
    server_address = 'ws://10.5.1.11:13338'  

    pseudo = input("Saisissez votre pseudo : ")

    async with websockets.connect(server_address) as websocket:
        try:
            greeting = f"Hello|{pseudo}"
            await websocket.send(greeting)

            task_send = asyncio.create_task(send_user_input(websocket))
            task_receive = asyncio.create_task(receive_server_messages(websocket))

            await asyncio.gather(task_send, task_receive)

        except KeyboardInterrupt:
            print("\nClient interrompu. Fermeture en cours...")

if __name__ == "__main__":
    asyncio.run(main())
