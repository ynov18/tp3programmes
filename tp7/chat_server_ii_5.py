import asyncio
import websockets

CLIENTS = {}

async def handle_client(websocket, path):
    try:
        client_address = websocket.remote_address
        print(f"Client connecté : {client_address}")

        data = await websocket.recv()
        if not data.startswith("Hello|"):
            print("Message de connexion invalide. Déconnexion du client.")
            return

        pseudo = data.split("|")[1]

        if client_address not in CLIENTS:
            CLIENTS[client_address] = {
                "ws": websocket,
                "pseudo": pseudo
            }

            announce_message = f"Annonce : {pseudo} a rejoint la chatroom\n"
            await broadcast_message(client_address, announce_message)

        while True:
            data = await websocket.recv()
            if not data:
                break

            message = data
            print(f"Message reçu de {pseudo} : {message}")

            await broadcast_message(client_address, f"{pseudo} a dit : {message}")

    except websockets.ConnectionClosedError:
        pass
    finally:
        print(f"Client déconnecté : {client_address}")
        del CLIENTS[client_address]

        await broadcast_message(client_address, f"Annonce : {pseudo} a quitté la chatroom\n")

async def broadcast_message(sender_address, message):
    for client_address, client_info in CLIENTS.items():
        if client_address != sender_address:
            await client_info["ws"].send(message)

async def main():
    server = await websockets.serve(
        handle_client, '10.5.1.11', 13338)

    async with server:
        await server.wait_closed()

if __name__ == "__main__":
    asyncio.run(main())
