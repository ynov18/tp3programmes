import os
import sys
import json
import hashlib
import datetime

PROGRAM_NAME = "ids"
ETC_FOLDER = "/etc"
CONFIG_FOLDER = os.path.join(ETC_FOLDER, PROGRAM_NAME)
DB_FILE_PATH = os.path.join(CONFIG_FOLDER, "db.json")
CONFIG_FILE_PATH = os.path.join(CONFIG_FOLDER, "config.json")

def create_config_folder():
    if not os.path.exists(CONFIG_FOLDER):
        os.makedirs(CONFIG_FOLDER)

def create_config_file():
    if not os.path.exists(CONFIG_FILE_PATH):
        default_config = {"files": ["/chemin/vers/fichier1.txt","/chemin/vers/fichier2.txt"],"folders": ["/chemin/vers/dossier1","/chemin/vers/dossier2"],"monitor_ports": True}
        with open(CONFIG_FILE_PATH, "w") as config_file:
            # Initialize the configuration file with the default values
            json.dump(default_config, config_file, indent=2)


def log_command(command):
    logs = [f"{datetime.datetime.now()} - Command: {command}"]
    
    with open(DB_FILE_PATH, "r") as db_file:
        stored_state = json.load(db_file)

    stored_logs = stored_state.get("logs", [])
    stored_logs.extend(logs)

    stored_state["logs"] = stored_logs

    with open(DB_FILE_PATH, "w") as db_file:
        json.dump(stored_state, db_file, indent=2)

def build_state(config_data):
    file_info = {}
    port_info = {}
    
    # Analyser les fichiers à surveiller
    for path in config_data.get("files", []):
        analyze_path(path, file_info)

    # Analyser les dossiers à surveiller (y compris les sous-dossiers)
    for path in config_data.get("folders", []):
        for root, dirs, files in os.walk(path):
            for file_name in files:
                file_path = os.path.join(root, file_name)
                analyze_path(file_path, file_info)

    state = {"file_info": file_info, "port_info": port_info, "logs": []}
    
    with open(DB_FILE_PATH, "w") as db_file:
        json.dump(state, db_file, indent=2)

    log_command("build")
    print("Build successful.")

def check_state(config_data):
    if not os.path.exists(DB_FILE_PATH):
        print("Error: Database file does not exist. Run 'build' command first.")
        sys.exit(1)

    with open(DB_FILE_PATH, "r") as db_file:
        stored_state = json.load(db_file)

    current_state = {"file_info": {}, "port_info": {}}

    # Analyser les fichiers à surveiller
    for path in config_data.get("files", []):
        analyze_path(path, current_state["file_info"])

    # Analyser les dossiers à surveiller (y compris les sous-dossiers)
    for path in config_data.get("folders", []):
        for root, dirs, files in os.walk(path):
            for file_name in files:
                file_path = os.path.join(root, file_name)
                analyze_path(file_path, current_state["file_info"])

    logs = stored_state.get("logs", [])
    # Capture the output of the check command
    output = json.dumps({"state": "ok"})
    try:
        import subprocess
        output = subprocess.check_output(["python", "ids.py", "check"]).decode("utf-8").strip()
    except subprocess.CalledProcessError as e:
        output = e.output.decode("utf-8").strip()
    
    logs.append({"check_output": output})
    
    logs.append("check")
    current_state["logs"] = logs
    
    if current_state == stored_state:
        print(json.dumps({"state": "ok"}))
    else:
        print(json.dumps({"state": "divergent", "changes": current_state}))
def analyze_path(path, file_info):
    if os.path.isfile(path):
        file_info[path] = get_file_info(path)

def get_file_info(file_path):
    file_stat = os.stat(file_path)

    md5_hash = hashlib.md5()
    with open(file_path, "rb") as f:
        for byte_block in iter(lambda: f.read(4096), b""):
            md5_hash.update(byte_block)

    sha256_hash = hashlib.sha256()
    with open(file_path, "rb") as f:
        for byte_block in iter(lambda: f.read(4096), b""):
            sha256_hash.update(byte_block)

    sha512_hash = hashlib.sha512()
    with open(file_path, "rb") as f:
        for byte_block in iter(lambda: f.read(4096), b""):
            sha512_hash.update(byte_block)

    modification_time = datetime.datetime.fromtimestamp(file_stat.st_mtime).strftime('%Y-%m-%d %H:%M:%S')
    creation_time = datetime.datetime.fromtimestamp(file_stat.st_ctime).strftime('%Y-%m-%d %H:%M:%S')

    owner_uid = file_stat.st_uid
    owner_name = str(owner_uid)  # Modification : affiche l'UID si le module pwd n'est pas disponible
    group_gid = file_stat.st_gid
    group_name = str(group_gid)  # Modification : affiche le GID si le module grp n'est pas disponible

    file_size = file_stat.st_size

    return {
        "MD5": md5_hash.hexdigest(),
        "SHA256": sha256_hash.hexdigest(),
        "SHA512": sha512_hash.hexdigest(),
        "Modification Time": modification_time,
        "Creation Time": creation_time,
        "Owner": owner_name,
        "Group": group_name,
        "File Size": file_size
    }

# Utilisation
create_config_folder()  # Crée le sous-dossier dans /etc pour le programme
create_config_file()  # Crée le fichier de configuration s'il n'existe pas
with open(CONFIG_FILE_PATH, "r") as config_file:
    config_data = json.load(config_file)

if len(sys.argv) != 2:
    print("Usage: python ids.py [build|check]")
    sys.exit(1)

command = sys.argv[1].lower()

if command == "build":
    build_state(config_data)
elif command == "check":
    check_state(config_data)
else:
    print("Invalid command. Use 'build' or 'check'.")
    sys.exit(1)

