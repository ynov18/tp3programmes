from socket import gethostbyname
from sys import argv
import os 
import psutil

def ping(adresse):
    if(os.system(f"ping {argv[2]} >NUL")==0):
        return("UP!")
    else  : 
        return("DOWN!")

def lookup(domaine):
    return gethostbyname(argv[2])

def ip():
    interfaces = psutil.net_if_addrs()
    for interface, addrs in interfaces.items():
        for addr in addrs:
            if addr.family == 2 and 'Wi-Fi' in interface:
                return addr.address


if(argv[1]=="ping"):
    result=ping(argv[2])
elif(argv[1]=="lookup"):
    result=lookup(argv[2])
elif(argv[1]=="ip"):
    result=ip()    
else:
    result=argv[1]+" is not an available command. Déso."
print(result)    